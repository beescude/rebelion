/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulacionfenomeno;

import extras.Simulacion;

/**
 *
 * @author Pc
 */
public class Carcel {
    private Agentes preso;
    private int tiempoCarcel;
    private static int maximoTiempoCarcel;
    private boolean regreso=false;
        //Creamos el constructor Carcel que recibe los agentes arrestados por los policias.

    public Carcel(Agentes arrestado){
        this.preso=arrestado;
        tiempoCarcel=0;
    }
        // Modificamos el tiempo de carcel que un ciudadanos pueda estar.

    public static void setMaximoTiempoCarcel(int maximoTiempoCarcel){
        Carcel.maximoTiempoCarcel = maximoTiempoCarcel;
    }
        // Retornamos los presos

    public Agentes getPreso(){
        return preso;
    }
        // Retornamos el tiempo de carcel

    public int getTiempoCarcel(){
        return tiempoCarcel;
    }
        // Retornamos el tiempo maximo de carcel

    public static int getMaximoTiempoCarcel(){
        return maximoTiempoCarcel;
    }
    // Creamos el metodo regresar Universo que recibe una matriz de los agentes ( policias y ciudadanos)

    public void regresarUniverso(Agentes[][] matriz){
        if (tiempoCarcel>maximoTiempoCarcel){
            int fila = (int)(Math.random()*matriz.length);
            int columna = (int)(Math.random()*matriz[0].length);
            while (matriz[fila][columna] != null){
                fila = (int)(Math.random()*matriz.length);
                columna = (int)(Math.random()*matriz[0].length);
            }
            matriz[fila][columna] = preso;
            preso.setN(fila);
            preso.setM(columna);
            Simulacion.setNumeroCiudadanos(Simulacion.getNumeroCiudadanos()+1);
            regreso=true;
        }
    }
        //Creamos el metodo de contar dias en la carcel

    public void contarDiaCarcel(){
        tiempoCarcel++;
    }
        // Retornamos un valor booleano si es que el ciudadanos encarcelado regresa a nuestro universo

    public boolean getRegreso(){
        return regreso;
    }
}
