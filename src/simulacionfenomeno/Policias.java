/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulacionfenomeno;

import extras.Simulacion;
import java.util.ArrayList;

/**
 *
 * @author Pc
 */
public class Policias extends Agentes{
     private boolean encarcelo = false;
          //creamos el constrcutor Policias que recibe una fila y columna

    public Policias(int n, int m){
        super(n,m);
    }
        // Creamos el metodo encarcelar  que recibe un arreglo de la lista de encarcelados para llenarlos y la matriz con todos los policias y ciudadanos.

    public void encarcelar(ArrayList<Carcel> presos,Agentes[][]matriz){
        ArrayList<Agentes> ciudadanos = new ArrayList<>();
        for(int f=0;f<super.getMatrizVision().length;f++){
            for (int c=0;c<super.getMatrizVision()[0].length;c++){
                if (super.getMatrizVision()[f][c] instanceof Ciudadanos){
                    Ciudadanos ciudadano = (Ciudadanos)super.getMatrizVision()[f][c];
                    if (ciudadano.getActivo()){
                        Agentes ciudadano1 = (Agentes)ciudadano;
                        ciudadanos.add(ciudadano1);
                    }
                }
            }
        }   
         //La lista ciudadanos son todos los ciudadanos activos dentro de la vision del policia y escogemos uno aleatorio para que el policia encarcele y se mueva
        if (ciudadanos.size()!=0){
            int pos = (int)(Math.random()*(ciudadanos.size()-1));
            Carcel preso = new Carcel(ciudadanos.get(pos));
            presos.add(preso);
            matriz[ciudadanos.get(pos).getN()][ciudadanos.get(pos).getM()] = matriz[super.getN()][super.getM()];
            matriz[super.getN()][super.getM()]=null;
            super.setN(ciudadanos.get(pos).getN());
            super.setM(ciudadanos.get(pos).getM());
            encarcelo = true;
            Simulacion.setNumeroCiudadanos(Simulacion.getNumeroCiudadanos()-1);
        }
    }
        //Retornamos los encarcelos

    public boolean getEncarcelo(){
        return encarcelo;
    }
         // Modificamos los encarcelados.

    public void setEncarcelo(boolean encarcelo){
        this.encarcelo = encarcelo;
    }
        // Retornamos el simbolo P para identificar que es un policia.

    public String toString(){ 
        return "P";
    } 
    
    
}
