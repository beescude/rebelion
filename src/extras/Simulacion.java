/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package extras;

import java.io.File;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Scanner;
import simulacionfenomeno.Agentes;
import simulacionfenomeno.Agentes;
import simulacionfenomeno.Carcel;
import simulacionfenomeno.Carcel;
import simulacionfenomeno.Ciudadanos;
import simulacionfenomeno.Ciudadanos;
import simulacionfenomeno.Policias;
import simulacionfenomeno.Policias;

/**
 *
 * @author Pc * Creamos la clase Simulacion donde implementamos las reglas
 * basicos del experimento , creamos sus correspondientes atributos y metodos.
 *
 */
public class Simulacion {

    private static int numeroCiudadanoRebelandose = 0;
    private static int numeroCiudadanoSinRebelarse = 0;
    private static boolean movimiento;
    private static Agentes[][] universo;
    private static int dias;
    private static int numeroCiudadanos;
    private static ArrayList<Carcel> presos = new ArrayList<>();
    private static int numeroPolicias;
    private static double legitimidadInicial;
    private static double legitimidadFinal;
    private static double legitimidadGobierno;
    private static final double Constante = 2.3;
    private static final double Limite = 0.1;
    private static Scanner sc = new Scanner(System.in);
    //Para crear la simulacion necesitamos una matriz y la densidad de porcentajes de la poblacion , con la vision de cada policias y los dias transcurridos del fenomeno.

    public Simulacion(int filas, int columnas, double porcentajeCiudadanos, double porcentajePolicias, int dias, double legitimidadInicial, double legitimidadFinal, boolean movimiento) {
        this.movimiento = movimiento;
        this.legitimidadInicial = legitimidadInicial;
        this.legitimidadFinal = legitimidadFinal;
       
       
        this.dias = dias;
        universo = new Agentes[filas][columnas];
        numeroCiudadanos = (int) (filas * columnas * porcentajeCiudadanos / 100);
        numeroPolicias = (int) (filas * columnas * porcentajePolicias / 100);
        for (int i = 0; i < numeroCiudadanos; i++) {
            int fila = (int) (Math.random() * filas);
            int columna = (int) (Math.random() * columnas);
            while (universo[fila][columna] != null) {
                fila = (int) (Math.random() * filas);
                columna = (int) (Math.random() * columnas);
            }
            Agentes ciudadano = new Ciudadanos(fila, columna);
            universo[fila][columna] = ciudadano;
        }
        for (int i = 0; i < numeroPolicias; i++) {
            int fila = (int) (Math.random() * filas);
            int columna = (int) (Math.random() * columnas);
            while (universo[fila][columna] != null) {
                fila = (int) (Math.random() * filas);
                columna = (int) (Math.random() * columnas);
            }
            Agentes policia = new Policias(fila, columna);
            universo[fila][columna] = policia;
        }
    }
    //Creamos nuestro Universo ( la matriz) , empezamos la simulacion , cada dia se crea una matriz , se mueven depediendo del move y los policias encarcelas a los ciudadanos si estos pasan del limite .

    public void empezarSimulacion() {
        System.out.println("");
        System.out.println("-------------UNIVERSO GENERADO-------------");
        System.out.println("Numero de Ciudadanos: " + numeroCiudadanos);
        System.out.println("Numero de Policias: " + numeroPolicias);
        System.out.println("");
        System.out.println("+: Ciudadanos Rebelandose");
        System.out.println("-: Ciudadanos sin Rebelarse");
        System.out.println("P: Policias");
        System.out.println("");
        Simulacion.calcularNumeroCiudadanosSinRebelarse();
        Simulacion.calcularNumeroCiudadanosRebelandose();
        Simulacion.imprimirMatriz(universo);
        System.out.println("");
        System.out.print("De ENTER para empezar la simulacion:");
        sc.nextLine();
        Simulacion.generarMatricesVision();
        for (int i = 0; i < dias; i++) {
            if (presos.size() != 0) {
                for (Carcel preso : presos) {
                    preso.contarDiaCarcel();
                    preso.regresarUniverso(universo);
                }
            }
            System.out.println("");
            System.out.println("------------- DIA: " + (i + 1) + " -------------");
            if (i == 0) {
                legitimidadGobierno = legitimidadInicial;
            } else {
                Simulacion.calcularLegitimidad();
            }
            for (int f = 0; f < universo.length; f++) {
                for (int c = 0; c < universo[0].length; c++) {
                    if (universo[f][c] != null) {
                        if (universo[f][c] instanceof Ciudadanos) {
                            Ciudadanos ciudadano = (Ciudadanos) universo[f][c];
                            ciudadano.calcularAgravio(legitimidadGobierno);
                            int cantidadCiudadanos = ciudadano.cantidadCiudadanosRebeldesVision();
                            int cantidadPolicias = ciudadano.cantidadPoliciasVision();
                            ciudadano.calcularProDetencionEstimada(Constante, cantidadPolicias, cantidadCiudadanos);
                            ciudadano.calcularRiesgoNeto();
                            ciudadano.verActividad(Limite);
                            universo[f][c] = ciudadano;
                            universo[f][c].setSeMovio(false);
                        } else if (universo[f][c] instanceof Policias) {
                            Policias policia = (Policias) universo[f][c];
                            policia.setEncarcelo(false);
                            universo[f][c] = policia;
                            universo[f][c].setSeMovio(false);
                        }
                    }
                }
            }
            if (presos.size() != 0) {
                ArrayList<Carcel> presos1 = new ArrayList<>(presos);
                for (Carcel preso : presos1) {
                    if (preso.getRegreso()) {
                        presos.remove(presos.indexOf(preso));
                    }
                }
            }
            //calculamos los numeros de ciudadanos activos e inactivos , imprimos nuestro matriz y recorremos nuestro lista de encarcelados para sacar los ciduadanos capturados.
            Simulacion.calcularNumeroCiudadanosRebelandose();
            Simulacion.calcularNumeroCiudadanosSinRebelarse();
            Simulacion.imprimirMatriz(universo);
            //Creamos la matriz vision de cada agente del universo
            Simulacion.generarMatricesVision();
            if (movimiento) {
                System.out.println("");
                System.out.print("De ENTER para que la poblacion se movilice: ");
                sc.nextLine();
                ArrayList<String> posUsadas = new ArrayList<>();
                for (int x = 0; x < (numeroCiudadanos + numeroPolicias); x++) {
                    int fila = (int) (Math.random() * universo.length);
                    int columna = (int) (Math.random() * universo[0].length);
                    String pos = fila + "," + columna;
                    while (universo[fila][columna] == null || posUsadas.contains(pos) || universo[fila][columna].getSeMovio()) {
                        fila = (int) (Math.random() * universo.length);
                        columna = (int) (Math.random() * universo[0].length);
                        pos = fila + "," + columna;
                    }
                    posUsadas.add(pos);
                    universo[fila][columna].movimento(universo);
                    Simulacion.generarMatricesVision();
                }
                System.out.println("La poblacion se ha movilizado.");
                Simulacion.calcularNumeroCiudadanosSinRebelarse();
                Simulacion.calcularNumeroCiudadanosRebelandose();
                Simulacion.imprimirMatriz(universo);
            }
            //Arrestamos a los ciudadanos activos
            ArrayList<String> posUsadas1 = new ArrayList<>();
            System.out.println("");
            System.out.print("De ENTER para que se arresten a los Ciudadanos Rebeldes: ");
            sc.nextLine();
            for (int x = 0; x < (numeroPolicias); x++) {
                int fila = (int) (Math.random() * universo.length);
                int columna = (int) (Math.random() * universo[0].length);
                String pos = fila + "," + columna;
                while (universo[fila][columna] == null || posUsadas1.contains(pos) || !(universo[fila][columna] instanceof Policias) || ((Policias) universo[fila][columna]).getEncarcelo()) {
                    fila = (int) (Math.random() * universo.length);
                    columna = (int) (Math.random() * universo[0].length);
                    pos = fila + "," + columna;
                }
                posUsadas1.add(pos);
                ((Policias) universo[fila][columna]).encarcelar(presos, universo);
                Simulacion.generarMatricesVision();
            }
            System.out.println("Se han arrestado a los Ciudadanos Rebeldes.");
            Simulacion.calcularNumeroCiudadanosSinRebelarse();
            Simulacion.calcularNumeroCiudadanosRebelandose();
            Simulacion.imprimirMatriz(universo);
            Simulacion.generarMatricesVision();
            if ((i + 2) > dias) {
                System.out.println("");
                System.out.print("De ENTER para Finalizar la Simulacion: ");
                sc.nextLine();
            } else {
                System.out.println("");
                System.out.print("De ENTER para pasar al DIA " + (i + 2) + ": ");
                sc.nextLine();
            }
            Simulacion.calcularNumeroCiudadanosRebelandose();
            Simulacion.calcularNumeroCiudadanosSinRebelarse();
            try {
                // Se inicialisa la clase para que de fecha y tiempo instantaneo
                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("ddMMyyyyHHmmss");
                LocalDateTime now = LocalDateTime.now();
                PrintWriter pw = new PrintWriter(new File("C:\\Users\\LuisitoAndrade\\Desktop\\ESPOL\\4 semestre\\Programacion Orientada a Objetos\\Proyecto//simulacion_" + dtf.format(now))); // Definir la ubicacion en donde se guardara el archivo
                StringBuilder sb = new StringBuilder();
                sb.append("-------------UNIVERSO GENERADO-------------");
                sb.append("\r\n").append("Numero de Ciudadanos:").append(numeroCiudadanos).append("\r\n");
                sb.append("Numero de Policias:").append(numeroPolicias).append("\r\n");
                sb.append("Numero de Dia: ").append(i+1).append("\r\n");
                sb.append("Numero de Ciudadanos sin Rebelarse: ").append(numeroCiudadanoSinRebelarse).append("\r\n");
                sb.append("Numero de Ciudadanos Rebelandose: ").append(numeroCiudadanoRebelandose).append("\r\n");
                sb.append("Numero de Ciudadanos en la Carcel:").append(presos.size()).append("\r\n");

                pw.write(sb.toString());
                pw.close();

             
            } catch (Exception e) {
            }
        }

    }
    //Calculamos la legimidad , cosa que puede ser una constante o entre un rango , si es una constrante la resta de legitimidad final  e inicial sera 0.
    public static void calcularLegitimidad() {
        double diferencial = (legitimidadFinal - legitimidadInicial) / (dias - 1);
        legitimidadGobierno += diferencial;
    }

    //Creamos el metodo imprimir Matriz 
    public static void imprimirMatriz(Agentes[][] matriz) {
        for (int x = 0; x < matriz.length; x++) {
            System.out.print("|");
            for (int y = 0; y < matriz[x].length; y++) {
                if (matriz[x][y] == null) {
                    System.out.print("O");
                } else {
                    System.out.print(matriz[x][y]);
                }
                if (y != matriz[x].length - 1) {
                    System.out.print("\t");
                }
            }
            System.out.println("|");
        }
        String carcel = "CARCEL:[||";
        for (Carcel preso : presos) {
            carcel = carcel + preso.getPreso() + "(" + preso.getTiempoCarcel() + " dia/s)" + "|";
        }
        carcel = carcel + "|] Maximo Tiempo de Carcel: " + Carcel.getMaximoTiempoCarcel() + " dia/s";
        System.out.println(carcel);
        System.out.println("Numero de Ciudadanos Rebelandose: " + numeroCiudadanoRebelandose);
        System.out.println("Numero de Ciudadanos sin Rebelarse: " + numeroCiudadanoSinRebelarse);
        System.out.println("Numero de Ciudadanos en la Carcel: " + presos.size());
    }

    public static void calcularNumeroCiudadanosSinRebelarse() {
        numeroCiudadanoSinRebelarse = 0;
        for (int f = 0; f < universo.length; f++) {
            for (int c = 0; c < universo[0].length; c++) {
                if (universo[f][c] != null) {
                    if (universo[f][c] instanceof Ciudadanos) {
                        Ciudadanos ciudadano = (Ciudadanos) universo[f][c];
                        if (!(ciudadano.getActivo())) {
                            numeroCiudadanoSinRebelarse++;
                        }
                    }
                }
            }
        }

    }

    // Creamos el metodo para calcular los ciudadanos no activos.
    public static void calcularNumeroCiudadanosRebelandose() {
        numeroCiudadanoRebelandose = 0;
        for (int f = 0; f < universo.length; f++) {
            for (int c = 0; c < universo[0].length; c++) {
                if (universo[f][c] != null) {
                    if (universo[f][c] instanceof Ciudadanos) {
                        Ciudadanos ciudadano = (Ciudadanos) universo[f][c];
                        if (ciudadano.getActivo()) {
                            numeroCiudadanoRebelandose++;
                        }
                    }
                }
            }
        }
    }
    //Creamos el metodo para generar la matriz vision del universo.

    public static void generarMatricesVision() {
        for (int f = 0; f < universo.length; f++) {
            for (int c = 0; c < universo[0].length; c++) {
                if (universo[f][c] != null) {
                    universo[f][c].obtenerMatrizVision(universo);
                }
            }
        }
    }

    // Retornamos el numero de ciudadanos
    public static int getNumeroCiudadanos() {
        return numeroCiudadanos;
    }
    // Modificamos el numero de ciudadanos.

    public static void setNumeroCiudadanos(int nCiudadanos) {
        numeroCiudadanos = nCiudadanos;
    }
}