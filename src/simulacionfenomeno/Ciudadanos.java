/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulacionfenomeno;

import static java.lang.Math.round;

/**
 *
 * @author Pc
 */
public class Ciudadanos extends Agentes {
    private double agravio;
    private double perjuicioPercibido;
    private double proDetencionEstimada;
    private double riesgoNeto;
    private double aversionRiesgo;
    private boolean activo = false;
           //Creanis nuestro constructor ciudadanos que recibe una fila y columna

    public Ciudadanos(int n, int m){
        super(n,m);
        perjuicioPercibido = Math.random()*1;
        aversionRiesgo = Math.random()*1;
    }
        //Calculamos el agravio de los ciudadanos
    public void calcularAgravio(double legitimidadGobierno){
        agravio = perjuicioPercibido*(1-legitimidadGobierno);
    }
        //Calculamos la cantidad de policia de acuerdo a la vision de la matriz

    public int cantidadPoliciasVision(){
        int numeroPolicias=0;
        for(int f=0;f<super.getMatrizVision().length;f++){
            for (int c=0;c<super.getMatrizVision()[0].length;c++)
                if (super.getMatrizVision()[f][c] instanceof Policias){
                    numeroPolicias++;
                }
        }
        return numeroPolicias;
    }
        // Claculamos la cantidad de ciudadanos rebeldes 

    public int cantidadCiudadanosRebeldesVision(){
        int numeroCiudadanos=0;
        for(int f=0;f<super.getMatrizVision().length;f++){
            for (int c=0;c<super.getMatrizVision()[0].length;c++)
                if (super.getMatrizVision()[f][c] instanceof Ciudadanos && ((Ciudadanos)super.getMatrizVision()[f][c]).getActivo()){
                    numeroCiudadanos++;
                }
        }
        return numeroCiudadanos;
    }
        //Calculamos la detencionEstimada de los ciudadanos

    public void calcularProDetencionEstimada(double constante,int policias,int ciudadanosRebeldes){
        if (ciudadanosRebeldes == 0){
            proDetencionEstimada = 1;    
        }else{
            proDetencionEstimada = 1 - Math.exp(- constante * (policias / ciudadanosRebeldes) );
        }   
    }
        // Calculamos el riesgo neto 

    public void calcularRiesgoNeto(){
        riesgoNeto = aversionRiesgo*proDetencionEstimada;
    }
        // creamos el metodo para verificar si los ciudadanos son activos.

    public void verActividad(double limite){
        activo = agravio-riesgoNeto>limite;
    }
        //Se van cambiando el estado de los ciudadanos

    public void setActivo(boolean activo){
        this.activo = activo;
    }
        // Retornamos los activos

    public boolean getActivo(){
        return activo;
    }
        //Retornamos lo que quieremos que salga cuando imprimimos nuestro objeto.

    public String toString(){
        if (activo){
            return "+";
        }else{
            return "-";
        }
    }
}
