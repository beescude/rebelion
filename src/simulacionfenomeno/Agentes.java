/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulacionfenomeno;

/**
 *
 * @author Pc
 */
public abstract class Agentes {
    private static int radio;
    private int n;
    private int m;
    private int n0MVision;
    private int m0MVision;
    private int nMVision;
    private int mMVision;
    private Agentes[][] matrizVision;
    private boolean seMovio=false;
        // Creamos el constructor Agentes que recibe una fila y columna

    Agentes(int n,int m){
        this.n = n;
        this.m = m;
    }
        // Creamos el metodo movimiento que recibe una matriz de los agentes 

    public void movimento(Agentes[][] matriz){
        int dimension = matrizVision.length*matrizVision[0].length;
        boolean hayEspacio=false;
        for (int f=0; f<matrizVision.length;f++){
            for(int c=0;c<matrizVision[0].length;c++){
                if (f!=nMVision && c!=mMVision){
                    if (matrizVision[f][c]==null){
                        hayEspacio=true;
                    }
                }
            }
        }
        if(hayEspacio){
            int fila = (int)(Math.random()*matrizVision.length);
            int columna = (int)(Math.random()*matrizVision[0].length);
            while (matrizVision[fila][columna] != null || (fila == nMVision && columna == mMVision)){
                    fila = (int)(Math.random()*matrizVision.length);
                    columna = (int)(Math.random()*matrizVision[0].length);
            }
            matriz[n0MVision+fila][m0MVision+columna] = matriz[n][m];
            matriz[n][m] = null;
            n = n0MVision+fila;
            m = m0MVision+columna;   
        }
        seMovio=true; 
    }   
        // Creamos el metodo para obtener la nueva matriz con el radio de vision

    public void obtenerMatrizVision(Agentes[][] matriz){
        int filaInicio = n-radio;
        int filaFinal = n+radio;
        int columnaInicio = m-radio;
        int columnaFinal = m+radio;
        if (filaInicio<0){
            filaInicio=0;
        }
        if (filaFinal>=matriz.length){
            filaFinal = matriz.length-1;
        }
        if (columnaInicio<0){
            columnaInicio=0;
        }
        if (columnaFinal>=matriz[0].length){
            columnaFinal=(matriz[0].length)-1;
        }
        
        int filas = (filaFinal - filaInicio)+1;
        int columnas = (columnaFinal - columnaInicio)+1;
        matrizVision = new Agentes[filas][columnas];
        for (int f=0; f<matrizVision.length;f++){
            for(int c=0;c<matrizVision[0].length;c++){
                if(f==0 && c==0){
                    n0MVision=filaInicio;
                    m0MVision=columnaInicio;
                }
                if ((f+filaInicio)== n && (c+columnaInicio)== m){
                    matrizVision[f][c] = null;
                    nMVision=f;
                    mMVision=c;
                }else{
                    matrizVision[f][c] = matriz[f+filaInicio][c+columnaInicio];
                }    
            }    
        }
    }
        // Modificamos el radio de vision

    public static void setRadio(int radio){
        Agentes.radio =radio;
    }
        // Retornamos el radio de vision

    public static int getRadio(){
        return radio;
    }
        // Modificamos las filas

    public void setN(int n){
        this.n = n;
    }
        // Retornamos las filas

    public int getN(){
        return n;
    }
        // Modificamos las columnas

    public void setM(int m){
        this.m = m;
    }
        //Retornamos las columnas 

    public int getM(){
        return m;
    }
        //Modificamos la matriz vision 

    public void setMatrizVision(Agentes[][] matrizVision){
        this.matrizVision = matrizVision;
    }
        // Retornamos la matriz vision de agentes

    public Agentes[][] getMatrizVision(){
        return matrizVision;
    }
        // Retornamos un valor booleanos para ver si los agentes se movieron

     public boolean getSeMovio(){
        return seMovio;
    }
         // Modificamos el valor booleanos de moverse

    public void setSeMovio(boolean seMovio){
        this.seMovio = seMovio;
    }
        // Retornamos cuando se imprima el objeto agentes.

    public String toString(){ 
        return "";
    }
    
    public String imprimir(){
        return null;
    }
}

