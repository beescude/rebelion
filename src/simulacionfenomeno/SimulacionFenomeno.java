/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulacionfenomeno;

import extras.Simulacion;
import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;
import java.time.format.DateTimeFormatter;
import java.time.LocalDateTime;
        
/**
 *
 * @author Pc
 */
public class SimulacionFenomeno {

    private static Scanner sc = new Scanner(System.in);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SimulacionFenomeno.pedirDatos();
    }

    // Creamos el metodo pedirDatos , donde pedimos al ususario el registro de los valores que necesitamos para la creacion del fenomeno
    public static void pedirDatos() {
        System.out.println("---------------SIMULACION DE FENOMENOS SOCIALES---------------");
        System.out.print("Ingrese la Densidad  Inicial de Policias en porcentaje(%): ");
        String porcentajePolicias = sc.nextLine();
        porcentajePolicias = porcentajePolicias.replaceFirst(",", ".");
        while (!(SimulacionFenomeno.isNumeric(porcentajePolicias)) || Double.parseDouble(porcentajePolicias) >= 100 || Double.parseDouble(porcentajePolicias) <= 0) {
            System.out.print("El valor ingresado no es valido ingrese solo NUMEROS en el RANGO de (1-99):");
            porcentajePolicias = sc.nextLine();
            porcentajePolicias = porcentajePolicias.replaceFirst(",", ".");
        }
        //Verificamos que el usuario ingrese los valores adecuados 
        double porcentajeP = Double.parseDouble(porcentajePolicias);
        System.out.print("Ingrese la Densidad  Inicial de Ciudadanos en porcentaje(%): ");
        String porcentajeCiudadanos = sc.nextLine();
        porcentajeCiudadanos = porcentajeCiudadanos.replaceFirst(",", ".");
        while (!(SimulacionFenomeno.isNumeric(porcentajePolicias)) || (Double.parseDouble(porcentajeCiudadanos) + porcentajeP) > 100 || Double.parseDouble(porcentajeCiudadanos) <= 0) {
            System.out.print("El valor ingresado no es valido ingrese solo NUMEROS en el RANGO de (1-" + (100 - porcentajeP) + "):");
            porcentajeCiudadanos = sc.nextLine();
            porcentajeCiudadanos = porcentajeCiudadanos.replaceFirst(",", ".");
        }
        double porcentajeC = Double.parseDouble(porcentajeCiudadanos);
        System.out.print("Ingresar Tamaño del Universo en formato de (filas,columnas): ");
        String dimension = sc.nextLine();
        while (!(dimension.contains(",")) || !((dimension.split(",")).length == 2) ||(dimension.contains("."))|| !(SimulacionFenomeno.isNumeric(dimension.split(",")[0])) || !(SimulacionFenomeno.isNumeric(dimension.split(",")[1])) || Integer.parseInt(dimension.split(",")[0]) > 50 || Integer.parseInt(dimension.split(",")[1]) > 50 || Integer.parseInt(dimension.split(",")[0]) <= 0 || Integer.parseInt(dimension.split(",")[1]) <= 0) {
            System.out.print("Los valores ingresados no son validos ingrese solo NUMEROS en formato(filas,columnas) y en RANGO de (1-50): ");
            dimension = sc.nextLine();
        }
        int filas = Integer.parseInt(dimension.split(",")[0]);
        int columnas = Integer.parseInt(dimension.split(",")[1]);
        System.out.print("Ingrese Radio de Vision para la Poblacion(Ciudadanos y Policias): ");
        String r = sc.nextLine();
        while (!(SimulacionFenomeno.isNumeric(r)) || r.contains(",") || r.contains(".") || Integer.parseInt(r) > 5 || Integer.parseInt(r) <= 0) {
            System.out.print("El valor ingresado no es valido ingrese solo NUMEROS en el RANGO de (1-5): ");
            r = sc.nextLine();
        }
        int radio = Integer.parseInt(r);

        System.out.print("Ingrese Numero de Turnos(Dias): ");
        String d = sc.nextLine();
        while (!(SimulacionFenomeno.isNumeric(d)) || d.contains(",") || d.contains(".") || Integer.parseInt(d) <= 0) {
            System.out.print("El valor ingresado no es valido ingrese solo NUMEROS ENTEROS: ");
            d = sc.nextLine();
        }
        int dias = Integer.parseInt(d);
        System.out.print("Ingrese Numero de Maximo de dias en Prison: ");
        String dp = sc.nextLine();
        while (!(SimulacionFenomeno.isNumeric(dp)) || dp.contains(",") || dp.contains(".") || Integer.parseInt(dp) <= 0) {
            System.out.print("El valor ingresado no es valido ingrese solo NUMEROS ENTEROS: ");
            dp = sc.nextLine();
        }
        int diasprision = Integer.parseInt(dp);
        System.out.println("");
        System.out.println("---Legitimidad del gobierno---");
        System.out.println("1-Valor unico.");
        System.out.println("2-Rango de valores.");
        System.out.print("Elija una opcion: ");
        String o = sc.nextLine();
        while (!(o.equals("1")) && !(o.equals("2"))) {
            System.out.print("El valor ingresado no es valido ingrese solo el numero de las opciones posibles:");
            o = sc.nextLine();
        }
        int opcion = Integer.parseInt(o);
        double legitimidadInicial = 0;
        double legitimidadFinal = 0;
        if (opcion == 1) {
            System.out.print("Ingrese Valor unico: ");
            String legitimidadI = sc.nextLine();
            legitimidadI = legitimidadI.replaceFirst(",", ".");
            while (!(SimulacionFenomeno.isNumeric(legitimidadI)) || Double.parseDouble(legitimidadI) > 1 || Double.parseDouble(legitimidadI) <= 0) {
                System.out.print("El valor ingresado no es valido ingrese solo NUMEROS en el RANGO de (0,1-1): ");
                legitimidadI = sc.nextLine();
                legitimidadI = legitimidadI.replaceFirst(",", ".");
            }
            legitimidadInicial = Double.parseDouble(legitimidadI);
            legitimidadFinal = legitimidadInicial;
        } else if (opcion == 2) {
            System.out.print("Ingrese rango inicial: ");
            String legitimidadI = sc.nextLine();
            legitimidadI = legitimidadI.replaceFirst(",", ".");
            while (!(SimulacionFenomeno.isNumeric(legitimidadI)) || Double.parseDouble(legitimidadI) > 1 || Double.parseDouble(legitimidadI) <= 0) {
                System.out.print("El valor ingresado no es valido ingrese solo NUMEROS en el RANGO de (0,1-1): ");
                legitimidadI = sc.nextLine();
                legitimidadI = legitimidadI.replaceFirst(",", ".");
            }
            legitimidadInicial = Double.parseDouble(legitimidadI);
            System.out.print("Ingrese rango Final: ");
            String legitimidadF = sc.nextLine();
            legitimidadF = legitimidadF.replaceFirst(",", ".");
            while (!(SimulacionFenomeno.isNumeric(legitimidadF)) || Double.parseDouble(legitimidadF) > 1 || Double.parseDouble(legitimidadF) <= 0) {
                System.out.print("El valor ingresado no es valido ingrese solo NUMEROS en el RANGO de (0,1-1)");
                legitimidadF = sc.nextLine();
                legitimidadF = legitimidadF.replaceFirst(",", ".");
            }
            legitimidadFinal = Double.parseDouble(legitimidadF);
        }
        boolean movimiento = false;
        System.out.println("");
        System.out.println("---Movimiento---");
        System.out.println("1-on.");
        System.out.println("2-off");
        System.out.print("Elija una opcion: ");
        String o1 = sc.nextLine();
        while (!(o1.equals("1")) && !(o1.equals("2"))) {
            System.out.print("El valor ingresado no es valido ingrese solo el numero de las opciones posibles:");
            o1 = sc.nextLine();
        }
        int opcion1 = Integer.parseInt(o1);
        if (opcion1 == 1) {
            movimiento = true;
        } else if (opcion1 == 2) {
            movimiento = false;
        }
        try {
             // Se inicialisa la clase para que de fecha y tiempo instantaneo
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("ddMMyyyyHHmmss");
            LocalDateTime now= LocalDateTime.now();
            PrintWriter pw = new PrintWriter(new File("C:\\Users\\LuisitoAndrade\\Desktop\\ESPOL\\4 semestre\\Programacion Orientada a Objetos\\Proyecto//parametros_"+dtf.format(now))); // Definir la ubicacion en donde se guardara el archivo
            StringBuilder sb = new StringBuilder();
            sb.append("---------------SIMULACION DE FENOMENOS SOCIALES---------------");
            sb.append("\r\n").append("Porcentaje de la densidad inicial de policias: ").append(porcentajeP).append("%").append("\r\n");
            sb.append("Porcentaje de la densidad inicial de Ciudadanos: ").append(porcentajeC).append("%").append("\r\n");
            sb.append("Tamaño del Universo en formato de (filas,columnas):").append(filas).append(",").append(columnas).append("\r\n");
            sb.append("Radio de Vision para la Poblacion(Ciudadanos y Policias): ").append(radio).append("\r\n");
            sb.append("Numero de Turnos(Dias):").append(dias).append("\r\n");
            sb.append("Numero de Maximo de dias en Prision: ").append(diasprision).append("\r\n").append("\r\n");
            sb.append("---Legitimidad del gobierno---").append("\r\n");
            if (opcion == 1) {
                sb.append("Valor unico: ").append(legitimidadFinal).append("\r\n").append("\r\n");
            } else if (opcion == 2) {
                sb.append("Rango: ").append("[").append(legitimidadInicial).append(",").append(legitimidadFinal).append("]").append("\r\n").append("\r\n");
            }
            sb.append("---Movimiento---").append("\r\n");
            if (opcion1 == 1) {
                sb.append("On").append("\r\n");
            } else if (opcion1 == 2) {
                sb.append("Off").append("\r\n");
            }
            pw.write(sb.toString());
            pw.close();
            
        } catch (Exception e) {
        }
        Carcel.setMaximoTiempoCarcel(diasprision);
        Agentes.setRadio(radio);
        Simulacion s1 = new Simulacion(filas, columnas, porcentajeC, porcentajeP,dias, legitimidadInicial, legitimidadFinal, movimiento);
        s1.empezarSimulacion();
    }

    // Creanis el metodo que retornara un valor booleanos si es que el ususario ingrese una candena de String en vez de un numero.
    private static boolean isNumeric(String cadena) {
        try {
            Double.parseDouble(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }
}
